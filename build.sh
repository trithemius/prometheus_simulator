#!/bin/bash

echo "building go binary"
# For release
GOOS=linux GOARCH=amd64 go build -o ./promsim_linux_amd64
GOOS=linux GOARCH=arm64 go build -o ./promsim_linux_arm64
GOOS=windows GOARCH=amd64 go build -o ./promsim_windows_amd64.exe
GOOS=darwin GOARCH=arm64 go build -o ./promsim_macos_arm64
GOOS=darwin GOARCH=amd64 go build -o ./promsim_macos_amd64