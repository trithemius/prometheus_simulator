package main

import (
	"math/rand"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Define a Summary metric with a "monitoring_metrics" label
var requestLatency = prometheus.NewSummaryVec(
	prometheus.SummaryOpts{
		Name:        "request_latency_seconds",
		Help:        "Latency of HTTP requests",
		ConstLabels: prometheus.Labels{"monitoring_metrics": "true"},
	},
	[]string{},
)

// Define a Counter metric with a "monitoring_metrics" label
var requestCounter = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name:        "request_total",
		Help:        "Total number of HTTP requests",
		ConstLabels: prometheus.Labels{"monitoring_metrics": "true"},
	},
	[]string{},
)

// Define a Gauge metric with a "monitoring_metrics" label
var currentUsers = prometheus.NewGaugeVec(
	prometheus.GaugeOpts{
		Name:        "current_users",
		Help:        "Current number of active users",
		ConstLabels: prometheus.Labels{"monitoring_metrics": "true"},
	},
	[]string{},
)

func simulateRequest() {
	// Simulate a random delay
	latency := rand.Float64()*(0.5-0.1) + 0.1

	// Record latency in the Summary metric
	requestLatency.WithLabelValues().Observe(latency)

	// Increment the request counter
	requestCounter.WithLabelValues().Inc()

	// Update the current number of active users
	currentUsers.WithLabelValues().Set(float64(rand.Intn(100) + 1))
}

func main() {
	// Register the Prometheus metrics with the default registry
	prometheus.MustRegister(requestLatency)
	prometheus.MustRegister(requestCounter)
	prometheus.MustRegister(currentUsers)

	// Start an HTTP server for Prometheus on a specific port
	http.Handle("/metrics", promhttp.Handler())
	go func() {
		http.ListenAndServe(":9000", nil)
	}()

	// Simulate periodic HTTP requests
	for {
		simulateRequest()
		time.Sleep(time.Second)
	}
}
