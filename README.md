# promsim - Prometheus Metric Simulator

`promsim` is a Prometheus metric simulator, written in Go, that provides a simple way to simulate Prometheus metrics for testing and development purposes. It exposes simulated metrics on port 9000, which can be accessed using `curl` or integrated into your Prometheus monitoring system.

## Installation

You can download the ZIP file containing all `promsim` binaries from the latest GitLab job artifacts for the project:

- [Download artifacts.zip (latest)](https://gitlab.com/trithemius/prometheus_simulator)

Once you've downloaded the ZIP file, extract it to your desired location. You will find the binaries for various operating systems and architectures inside the extracted folder.

## Run

To start the `promsim` server, use the appropriate binary for your architecture:

### Windows (AMD64)

```bash
./promsim_windows_amd64.exe
```

### Linux (AMD64)

```bash
./promsim_linux_amd64
```

### Linux (ARM64)

```bash
./promsim_linux_arm64
```

### macOS (AMD64)

```bash
./promsim_macos_amd64
```

### macOS (ARM64)

```bash
./promsim_macos_arm64
```

You can access the metrics using curl as follows:
```bash
curl http://localhost:9000/metrics
```
